package examen;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window extends JFrame {

    JTextField textField1;
    JTextField textField2;
    JTextField textField3;
    JButton button;

    Window() {
        setTitle("Exercitiul 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 500);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        textField1 = new JTextField();
        textField1.setBounds(10, 10, 300, 20);
        add(textField1);

        textField2 = new JTextField();
        textField2.setBounds(10, 30, 300, 20);
        add(textField2);

        textField3 = new JTextField();
        textField3.setBounds(10, 50, 300, 20);
        textField3.setEditable(false);
        add(textField3);

        button = new JButton("SUMA");
        button.setBounds(10, 70, 100, 100);
        button.addActionListener(new TratareButon());
        add(button);
    }

    public class TratareButon implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(button)) {
                String s1 = textField1.getText();
                String s2 = textField2.getText();
                int k1 = 0;
                int k2 = 0;
                for (int i = 0; i < s1.length(); i++) {
                    k1++;
                }
                for (int i = 0; i < s2.length(); i++) {
                    k2++;
                }
                int k;
                k = k1 + k2;
                textField3.setText(String.valueOf(k));
            }

        }
    }

    public static void main(String[] args) {

        Window w = new Window();

    }

}


